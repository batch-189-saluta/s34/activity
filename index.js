const express = require('express')

const app = express()

const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// ROUTES
app.get('/home', (request,response) => {
	response.send('Welcome to the home page')
})


// -----------------------------------------

let users = [
{
	username: "francis",
	password: "francis123"
},
{
	username: "johndoe",
	password: "johndoe123"
}
]

app.get('/users', (request,response) => {
	response.send(users)
})

// -------------------------------------------


app.delete('/delete-user', (request,response) =>{

	let message

	for (let i=0; i<users.length; i++){
		if(request.body.username == users[i].username){

			message = `user ${request.body.username} has been deleted`

			break
		} else {
			message = 'user does not exist'
		}
	}
	response.send(message)
})

app.listen(port, ()=> console.log(`Server is running at port: ${port}`))